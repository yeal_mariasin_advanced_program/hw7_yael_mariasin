﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace HW7
{
    public partial class Form1 : Form
    {
        DictionaryEntry[] de = new DictionaryEntry[52];
        NetworkStream clientStream;
        System.Windows.Forms.PictureBox oppoPic;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            changeDisablity(false);
            
            Thread thread = new Thread(new ThreadStart(connectToServer));
            thread.Start();
            loadInToDe();
        }

        public void loadInToDe()
        {
            int place = 0;
            ResourceSet resourceSet = Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                string resourceKey = entry.Key.ToString();
                Image resource = (Image)entry.Value;

                if (resourceKey != "card_back_blue" && resourceKey != "card_back_red")
                {
                    de[place].Key = resourceKey;
                    de[place].Value = resource;
                }
                else
                {
                    place--;
                }
                

                place++;
            }
        }

        public void GenerateEmptyCards()
        {
            Point nextLocation = new Point(48, 400);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::HW7.Properties.Resources.card_back_blue;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(75, 85);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                currentPic.Click += new EventHandler(onClickPicBox); //creating an event

                // assign an event to it
                /*currentPic.Click += delegate(object sender1, EventArgs e1) 
                                        {
                                            string currentIndex = currentPic.Name.Substring(currentPic.Name.Length-1);
                                            MessageBox.Show("You've clicked card #" + currentIndex);

                                            // use the delegate's params in order to remove the specific image which was clicked
                                            ((PictureBox)sender1).Hide();
                                            ((PictureBox)sender1).Dispose();
                                       };*/
                
                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 15;
            }

            nextLocation.X = 380;
            nextLocation.Y = 100;
            oppoPic = new PictureBox();
            oppoPic.Name = "picDynamic" + 11; // in our case, this is the only property that changes between the different images
            oppoPic.Image = global::HW7.Properties.Resources.card_back_red;
            oppoPic.Location = nextLocation;
            oppoPic.Size = new System.Drawing.Size(200, 228);
            oppoPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            this.Controls.Add(oppoPic);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "Confirm exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        public void connectToServer()
        {
            TcpClient client = new TcpClient();

            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint); 
            
            clientStream = client.GetStream(); 
            
            byte[] buffer = new ASCIIEncoding().GetBytes("I'm ready!"); 
            
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            
            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);

            if (buffer[0] == '0' && buffer[1] == '0' && buffer[2] == '0' && buffer[3] == '0')
            {
                MessageBox.Show("The game starts");
                GenerateEmptyCards();
                changeDisablity(true);
            }
            
        }

        public void changeDisablity(bool val)
        {
            foreach (var pB in Controls.OfType<PictureBox>())
            {
                //if (pictureBox.Tag == null) // you can skip other pictureBoxes
                    //continue;

                //int imageIndex = (int)pictureBox.Tag;
                //pictureBox.Image = imageList.Images[imageIndex];


                pB.Enabled = val;
            }
        }

        public void onClickPicBox(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int choice = rnd.Next(1, 53);

            System.Windows.Forms.PictureBox selectedPic = (PictureBox)sender;

            selectedPic.Image = (Image)de[choice].Value;
            selectedPic.Enabled = false;

            byte[] buffer = new ASCIIEncoding().GetBytes(de[choice].Key.ToString());

            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();

            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            
            for(int i = 0; i < de.Length; i++)
            {
                if(de[i].Key == buffer.ToString())
                {
                    oppoPic.Image = (Image)de[i].Value;
                    break;
                }
            }
        }

        
    }
}
